---
layout: page
title: About
permalink: /about/
---


*Critical Mission* is a cooperative starship bridge simulator game currently under development. It will be played by multiple players in a local network and is inspired by the [Artemis Spaceship Bridge Simulator](http://artemis.eochu.com/){:target="_blank"}
 and [Empty Epsilon](http://daid.github.io/EmptyEpsilon/){:target="_blank"}
.

Unlike both inspiring games, *Critical Mission* does not require the players to install a game client. Instead, only one machine must have the server software installed. The game itself runs inside the browser.


## Requirements

The computer that hosts the server has to support Java 8.
Client computers should have a recent version of Firefox or Chrome installed.

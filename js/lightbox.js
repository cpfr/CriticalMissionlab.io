

function Lightbox(element) {
    var _imgs = [];
    var _lightbox = document.getElementById("lightbox");
    var _lightboxContent = document.getElementById("lightboxContent");
    var _currentIdx = 0;

    var imgIdx = 0;
    for(var i = 0; i < element.childNodes.length; i++) {
        if(element.childNodes[i].tagName == "IMG") {
            var img = element.childNodes[i]
            _imgs.push(img);
            img.onclick = createClickHander(imgIdx);
            imgIdx++;
        }
    }

    if(!_lightbox) {
        _lightbox = document.createElement("div");
        _lightbox.id = "lightbox";

        _lightboxContent = document.createElement("div");
        _lightboxContent.id = "lightboxContent";

        var prev = document.createElement("span");
        var next = document.createElement("span");
        prev.textContent = "❮";
        next.textContent = "❯";
        prev.onclick = showPrev;
        next.onclick = showNext;

        _lightbox.appendChild(prev);
        _lightbox.appendChild(_lightboxContent);
        _lightbox.appendChild(next);

        document.body.appendChild(_lightbox);
    }

    function hideLightbox() {
        _lightbox.style.display = "none";
    }

    function showLightbox(idx) {
        var folderSepIdx = _imgs[idx].src.lastIndexOf("/");
        var bigUrl = _imgs[idx].src.substr(0, folderSepIdx)
                        + "/big" + _imgs[idx].src.substr(folderSepIdx);
        _currentIdx = idx;

        while(_lightboxContent.firstChild) {
            _lightboxContent.firstChild.remove();
        }
        var img = document.createElement("img");
        img.src = bigUrl;
        img.onclick = hideLightbox;
        _lightboxContent.appendChild(img);
        _lightbox.style.display = "block";
    }

    function showPrev() {
        var idx = _currentIdx-1;
        if(idx < 0) idx = _imgs.length-1;
        showLightbox(idx);
    }

    function showNext() {
        var idx = _currentIdx+1;
        if(idx > _imgs.length-1) idx = 0;
        showLightbox(idx);
    }

    function createClickHander(idx) {
        return function() { showLightbox(idx); }
    }

    this.show = showLightbox;
    element.lightbox = this;
}

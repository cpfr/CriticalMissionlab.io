---
layout: post
title:  "Wiring up things together - using events"
date:   2018-01-23 07:12:00 +0100
categories: implementation, technology
comments: true
---

In the last post I described my thoughts on the *Entity Component System* pattern. Today I'm going to talk about how the ECS that runs on the server interacts with the client-side logic inside the player's browser.

To summarize: All game objects (ships, asteroids, etc.) in the simulation are called *entities*. Entities consist of different types of *components* that contain the entity's data (position, speed, shield frequency...). Finally, *systems* iterate over those components in order to perform the game logic by manipulating the component's properties.

After implementing the core mechanics of this pattern, the next step was to connect the client to the server. The goal was to take the player's input and use it to change the simulation state.

## How Does Input Fit Into ECS?

This is a commonly asked question on *Stack Overflow*[^so]. Many people are asking how to integrate player input into an ECS-based game (I stumbled across them, asking myself the same question). However, in the case of *CriticalMission*, the architecture turns out to be a little bit more complicated because of the separation of client and server code.

The client-to-server direction is pretty straight-forward to implement, since this is the natural way HTTP works: The client sends a request to the server and receives a response in turn. In my implementation, the input is received by the *JavaScript* code inside the client's browser. It performs AJAX requests for every key press or button click that is bound to any action (for instance activating *red alert*). The opposite direction is implemented via *WebSockets*. This way, the server is able to send updates to the client, whenever this is necessary (e.g. when the speed of a ship changes or someone else hit the *red alert* button). The following figure shows the rough structure of the client code:

![client structure]({{site.baseurl}}/img/client-structure.svg)

I tried to split the client logic into different modules that implement the different concerns. The foundation is a library module that handles AJAX requests, WebSockets, input and events (I will explain that later on). The other two main modules are the simulation module that runs the actual game (moving ships around) and the application module that implements the game and player management like starting new simulations and joining players to a game. Further, there is a developer console, similar to that one in Quake3-based games like *"Star Trek: Voyager – Elite Force"*[^ef] or *"Star Wars - Jedi Knight II: Jedi Outcast"*[^jk2] (I mean the console where you enter the cheat codes -- I guess we all did that in order to get a purple lightsaber...). Finally, the figure contains a box with the label *"administration"*. This is the - not yet implemented - but planned admin interface that allows the user to install mods and plugins in order to customize their game experience. Examples for plugins could be different pieces of background music, new scenarios or custom ships. I try to always consider moddability during the development, while the actual implementation of the plugin system will have to wait until the basic functionality is running.

The different modules allow for a separation of concerns and increase the maintainability of the code. On the other hand, they also introduce a new problem: They need to communicate with each other and with the server. However, direct coupling between the different parts would reduce the benefit of modularization. One particular problem is the WebSocket client. For the sake of simplicity, there is only one WebSocket connection per client. However, the application module and the simulation module both need it in order to get updates from the server. Putting logic of both modules into the WebSocket implementation is a rather bad idea. The WebSocket client is a part of the library module and therefore has to be unaware of the implementation of the simulation and application modules.

The same problem applies to the server-side code: The HTTP resources are implemented in certain handler methods that belong to the Jetty-Part of the server. There is no direct connection to neither systems nor components.


## Events To The Rescue

Events are a great pattern for decoupling (by writing this I relize that avoiding tight coupling is one of my primary concerns, because loose coupling makes modding easier). There are different approaches on how to implement them and I chose two slightly different approaches for different use cases. Further, the implementation differs between the server the client.

### The Server-Side

The server can host multiple games at once. Therefore, there is a global event broker, for general messages, as well as an event broker per game. The per-game event broker is responsible for all events that are not of interest for anything that is not part of the particular game.

The server receives requests from the clients and the corresponding HTTP handlers generate events for the ECS. The systems (the *S* in ECS) can subscribe to certain event types. Every event type is implemented as a *Java* class. Further, every event subscriber has a message box, that is filled by the event broker, once an event occurs.

Let's take the *red alert* button as an example:

- The client calls the resource `POST /api/games/{gameId}/sim/{entityId}/tactical/redAlert`
- The corresponding HTTP handler on the server-side checks whether the particular game exists and creates an instance of `SetRedAlertEvent(entityId)` and fires it into the event broker of the game.
- The server responds with `HTTP 200: Accepted`[^http]
- The event broker distributes a copy of the event object to the message box of every subscriber (in fact, the event object is not copied, but since the events are immuatble, this does not really make a difference).
- In the next iteration of the update loop, the system that is responsible for *red alert* first checks all its messages in its box and then updates all entites that are assigned to it.
- During the processing of the events and entity updates, new events are published: In this case a `RedAlertActivatedEvent(entityId)`. This event, in turn can be received by other systems, or the client. There is a class `WebSocketEventHandler` that performs the translation from server to client events.

### The Client-Side

Clients can subscribe to server-side events using the following resource:

`POST /api/games/{gameId}/events/{eventName}/subscribe`

The above call causes the server to create a *WebSocketEventHandler* for that particular event type and client. The *WebSocketEventHandler* itself subscribes to the server-side event. Once it receives a message, it is serialized and sent via the web socket connection. In contrast to the other event subscribers, it does not contain a message box, but it translates the events as they occur (i.e. sending the event to the client is performed immediately and does not depend on any update cyle).

The WebSocket client receives the events from the server and hands them over to the event broker of the client. The events are then distributed to the subscribers on the client. In our example this could be any station:

- A `RedAlertActivatedEvent(entityId)` is received by the WebSocket client
- The WebSocket client deserializes the event and inserts it into the event broker.
- Since the main screen and the tactical station are subscribers for this type of event, they are notified.
- The the stations react to the event and causes the screeen to become red and an alarm sound is played.

## Summing Up

I described why I think that events are important for the realization of *CriticalMission*. There are two different kinds of event subscribers: First, subscribers that have a message box and actively check their message box for new events. Second, there are subscribers whose code is executed immediately when an event is received.

The *red alert* example shows a pretty extensive round-trip of an event. However, this way, every client is notified about the state change. Futher, this pattern is useful for more complex maneuvers like docking on a space station.

I hope you enjoyed reading this post, providing you an insight into the structure of the client and server implementation of *CriticalMission*.

Best regards,<br/>
Carsten

<br/>

[^so]: Stack Overflow<br/> [https://stackoverflow.com/](https://stackoverflow.com/){:target="_blank"}

[^ef]: Star Trek: Voyager – Elite Force<br/>  [https://en.wikipedia.org/wiki/Star_Trek:_Voyager_-_Elite_Force](https://en.wikipedia.org/wiki/Star_Trek:_Voyager_-_Elite_Force){:target="_blank"}

[^jk2]: Star Wars - Jedi Knight II: Jedi Outcast<br/> [https://en.wikipedia.org/wiki/Star_Wars_Jedi_Knight_II:_Jedi_Outcast](https://en.wikipedia.org/wiki/Star_Wars_Jedi_Knight_II:_Jedi_Outcast){:target="_blank"}

[^http]: List of HTTP status codes<br/> [https://en.wikipedia.org/wiki/List_of_HTTP_status_codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes){:target="_blank"}

---
layout: post
title:  "Choosing A Technology Stack"
date:   2017-11-04 08:12:00 +0100
categories: implementation, technology
comments: true
---

# Starting With Decisions...

Everytime I start a project, I have two main problems that are basically two decisions:

1. How should I name it?
2. What technology should I use to implement it?

Ironically, the first question usually turns out to be the hardest one. I can spend hours by researching ideas for interesting names until I am able to name that folder I want to create on my hard disc. In this case, it was relatively easy. I wanted to find a name that makes the player feel important. In a cooperative game, every player has a certain role and serves a specific purpose. I wanted to send the players on an important mission -- on a *critical mission*.

The second question is often not as hard to answer. In my last blog post, I already said that *Critical Mission* should run inside the user's browser. Hence HTML, CSS and JavaScript will be the way to go (I really hope that nobody would consider *Flash* as an alternative nowadays). However, there's not only the client-side, but also the server software. The server has to distribute the HTML files to the clients, run the simulation and manage the inputs of all clients. Using a real web server with a scripting module does not come into question. It is important that the game is easily runnable on a player's computer. Installing (and configuring) an *Apache*[^apache] web server is not really what one could expect from a player. The alternative is an embedded web server library that runs inside the game process. Since I am kind of a *Python*[^python] guy, the first possibilities that came into my mind were *Tornado*[^tornado] and *Flask*[^flask], two HTTP server libraries for the *Python* programming language. Although both options would have been suitable for implementing a game server, the *Python* runtime is only installed by default on *Linux* systems. In contrast to that, almost every PC user has a *Java Runtime Environment* (JRE) installed, so it would be easy for them to run a `.jar` file. This is why I chose to implement the game server in *Java*[^java].

# The Game Server
I remembered that we used a pretty decent *Java* HTTP server library called *Jetty*[^jetty] in a project at university. Jetty can be either used as a standalone web server or as an embedded HTTP library. We used it to implement the server-side API for our disaster alterting platform *mobile4D*[^mobile4d]. The game server serves three basic purposes: First, it delivers the client code - that is the HTML and CSS files, as well as the *JavaScript* code that implements the client logic. Second, it provides a *RESTful*[^rest] API that is used by the client code to change the state of the starship simulation (e.g. accellerating a ship). Finally, the server has to inform all connected clients about state changes. For example, when a starship changes its direction, all players should notice the course change on their display. There are basically three options for implementing server-to-client notifications on a website:

1. *Polling*
2. *Long-Polling*
3. *WebSockets*

*Polling* means that the client frequently asks the server for updates. The advantage of this approach is that it is simple to implement: The server provides a resource for retrieving the current state and the client repetitively uses that resource to verify that its data is still up-to-date. The disadvantage is that there are many requests necessary in order to keep the gaps between updates small so that the client does not operate on old data.

In contrast to regular polling, when using *long-polling* a client does not expect an immediate answer to a request. Instead, the connection is left open until the server has something new for the client. If the HTTP request runs into a timeout, the client starts a new request, assuming that the server did not have any updates within that time frame. The default timeout for HTTP requests in most browsers is 120 seconds. The advantage is that there are less unnecessary polling attempts, since the server only answers when there is something new. Further, the update is sent just-in-time (since there is already a connection open) and we don't have to wait for the next polling interval. Also there is no specific client-side technology needed, since this works with plain HTTP requests. The implementation of *long-polling* is mostly done on the server-side (i.e. keeping the connection open and sending the answer as soon as there are updates).

The newest solution for server-sent notifications is a technology called *WebSockets*. Instead of using HTTP requests for updates, the client creates a second, permanent connection to the server. This connection can be used similar to a plain TCP socket. That means that data can be sent in both directions (i.e. there is no request-response pattern as it is in HTTP). Since this technology is the newest one on the list and it must be supported by both, the HTTP server and the web browser, there have been some compatibility issues with older browsers in the past. Nowadays most chat websites and social platforms use *WebSockets* with a fallback implementation using *long-polling* for notifications. Since the browser support for *WebSockets* continually improves, I think that *long-polling* as a workaround will mostly vanish in the future. There is also a StackOverflow question[^so] about whether to use *WebSockets* or *long-polling*.

For *CriticalMission* I decided to go the *WebSockets* route without implementing a fallback solution. On the one hand, there are in particular older mobile devices that have problems with web sockets and it would be great to support them as player devices. On the other hand, *WebSocket* support will have improved even more when this game is finished and I don't want to waste time on implementing legacy technologies. If the game is finished and there is still a problem with browsers not supporting *WebSockets*, it should be possible to add a fallback solution afterwards. For now, I will try to actually make the game playable.


# The Client Implementation
The UI of the game is implemented in plain HTML and styled with *Sass*[^sass]-generated CSS. *Sass* is a small language that adds some useful features to CSS. The client-side logic is implemented in *JavaScript*. The client-side scripts communicate with the server via *Ajax*[^ajax] and *WebSockets*. The content-format for game data is *JSON*[^json] - a simple, but powerful alternative to XML.

A starship simulation doesn't get along without the iconic main view screen that serves as a kind of front window of the ship. Most *Artemis* or *Empty Epsilon* players use a big TV screen or a projector as the main screen. It provides a 3D view into the current scene and helps to enhance the immersion of being on a starship. Due to *WebGL*[^webgl] it is possible to render 3D scenes inside modern browsers. Since *WebGL* provides a mere low-level access to 3D rendering, I chose to use a third party library to cover up the plumbing work - and I found *Three.js*[^three]. There may or may not be better libraries - however, this was the first one that I tried that worked for me. You can see some starship renderings I have done using *Three.js* on the [media page]({{ site.baseurl }}/media).

# The Build Tool
First, I used *Maven*[^maven] for building the *Java* part of the game. At a first glance it worked okayish, but when I tried to also assemble the client-side code (generating CSS from *Sass*, combining multiple *JavaScript* files to a single one, packaging the game, etc.) I realized that it was not flexible enough for my purposes. Editing these clumsy XML configuration files was a pain. Then I decided to give *Gradle*[^gradle] a try. *Gradle* is compatible to *Maven* repositories, but the configuration of the gradle project tuned out to be much simpler (in particular if you understand the *Groovy* programming language[^groovy]).

# Content Creation Tools
Aside from writing and compiling the code, also the game content has to be created. Without any further explaination, the following list shows tools I use for content creation:

- 2D graphics: *Gimp*[^gimp] and *Inkscape*[^inkscape]
- 3D graphics: *Blender*[^blender]
- coding: *Atom[^atom] and *IntelliJ Community Edition*[^intellij]
- audio and music: *Audacity*[^audacity] and *LMMS*[^lmms]

# Summing Up

To conclude, I have found the name for this project surprisingly quick and I have made some decisions regarding the tools that I was going to use. Since I am not a friend of big design up front, none of these decisions is cast in stone. Tool selection is, however, an important topic, because in my opinion, nobody should be forced to mess with the tools instead of messing with the real problems (i.e. implementing software). This article only summarizes the technology that I have already used within this project. Do you have any questions or suggestions for better tools or techniques I should look up? Please let me know in the comments!

Best regards,<br/>
Carsten

<br/>

[^ajax]: Ajax (Wikipedia)<br/> [https://en.wikipedia.org/wiki/Ajax_(programming)](https://en.wikipedia.org/wiki/Ajax_(programming)){:target="_blank"}
[^apache]: Apache<br/> [https://httpd.apache.org/](https://httpd.apache.org/){:target="_blank"}
[^flask]: Flask - A Python Microframework<br/> [http://flask.pocoo.org/](http://flask.pocoo.org/){:target="_blank"}
[^gradle]: Gradle Build Tool<br/> [https://gradle.org/](https://gradle.org/){:target="_blank"}
[^groovy]: The Apache Groovy Programming Languaga<br/> [http://groovy-lang.org/](http://groovy-lang.org/){:target="_blank"}
[^java]: Java<br/> [https://www.java.com/](https://www.java.com/){:target="_blank"}
[^jetty]: Jetty - Servlet Engine And HTTP Server<br/> [https://www.eclipse.org/jetty/](https://www.eclipse.org/jetty/){:target="_blank"}
[^json]: JSON (JavaScript Object Notation)<br/> [http://json.org/](http://json.org/){:target="_blank"}
[^maven]: Apache Maven<br/> [https://maven.apache.org/](https://maven.apache.org/){:target="_blank"}
[^mobile4d]: mobile4D - Mobile Solutions For Emerging Countries<br/> [http://mobile4d.capacitylab.org/](http://mobile4d.capacitylab.org/){:target="_blank"}
[^python]: Python<br/> [https://www.python.org/](https://www.python.org/){:target="_blank"}
[^rest]: REST - Representational State Transfer<br/> [https://en.wikipedia.org/wiki/Representational_state_transfer](https://en.wikipedia.org/wiki/Representational_state_transfer){:target="_blank"}
[^sass]: Sass: Syntactically Awesome Style Sheets<br/> [http://sass-lang.com/](http://sass-lang.com/){:target="_blank"}
[^so]: StackOverflow - In what situations would AJAX long/short polling be preferred over HTML5 WebSockets?<br/> [https://stackoverflow.com/questions/10028770/in-what-situations-would-ajax-long-short-polling-be-preferred-over-html5-websock](https://stackoverflow.com/questions/10028770/in-what-situations-would-ajax-long-short-polling-be-preferred-over-html5-websock){:target="_blank"}
[^three]: Three.js - JavaScript 3D library<br/> [https://threejs.org/](https://threejs.org/){:target="_blank"}
[^tornado]: Tornado Web Server<br/> [http://www.tornadoweb.org/](http://www.tornadoweb.org/){:target="_blank"}
[^webgl]: WebGL<br/> [https://en.wikipedia.org/wiki/WebGL](https://en.wikipedia.org/wiki/WebGL){:target="_blank"}

[^gimp]: GIMP - Gnu Image Manipulation Program<br/> [https://www.gimp.org/](https://www.gimp.org/){:target="_blank"}
[^inkscape]: Inkscape - Draw Freely <br/> [https://inkscape.org/](https://inkscape.org/){:target="_blank"}
[^blender]: Blender - Free & Open Source 3D Creation Suite<br/> [https://www.blender.org/](https://www.blender.org/){:target="_blank"}
[^atom]: Atom - A Hackable Text Editor For The 21st Century <br/> [https://atom.io/](https://atom.io/){:target="_blank"}
[^intellij]: InteliJ IDEA: The Java IDE For Professional Developers<br/> [https://www.jetbrains.com/idea/](https://www.jetbrains.com/idea/){:target="_blank"}
[^audacity]: Audacity - Free, Open Source Cross-Platform Audio Software<br/> [http://www.audacityteam.org/](http://www.audacityteam.org/){:target="_blank"}
[^lmms]: LMMS - Open Source Digital Audio Workstation<br/> [https://lmms.io/](https://lmms.io/){:target="_blank"}

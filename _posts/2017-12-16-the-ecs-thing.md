---
layout: post
title:  "The ECS Thing"
date:   2017-12-16 07:12:00 +0100
categories: implementation, technology
comments: true
---

# Moving A Ship Around

In the last two posts I talked about the overall idea and the tech stack. Today I'm going to talk about an architectural pattern: *Entity-Component-System* (ECS).
After implementing the basic HTTP-server functionality using *Jetty*, as well as prototyping some screens and 3D models, I had to connect the loose ends.
This raised a question: How to manage the entities (ships, asteroids, planets, space stations)? A goal for *CriticalMission* is to provide a playground for the players that is as flexible as possible,
because the game experience on your starship bridge highly depends on the level of immersion that is achieved. On the one hand, casual players should be
able to run quick games just for fun. On the other hand, role-players should be able to invent their own types of ships, their own species and user interface.

The first straight-forward approach that comes into every OO-programmer's mind is to implement a class for each type of entity. Similarities between different types of entities could be implemented using inheritance:

![inheritance hierarchy]({{site.baseurl}}/img/inheritance.png)

This, however, raises another interesting question: How to deal with cases where two non-related object types share a common feature? In the example above: What if an asteroid is hollow and contains some cargo that can be collected by the player? Moving the *Cargo* property into *SpaceObject* is certainly not a good solution. Multiple inheritance? This might work in *C++*, but it's not possible in *Java*. Further, it introduces some other problems which we don't want to struggle with. Also, what do we do if a *Ship* has no *BeamWeapons*? The answer is that inheritance is not the best solution for this problem. Instead of inheriting features in order to reuse code, we could use composition. There are many articles on this topic around the web. One example is the  *Cowboy Programming* post *Evolve your hierarchy*[^cowboy]. Another one is the *Component* section in Robert Nystrom's book *Game Programming Patterns* that you can read online for free[^gpp]. This blog post is not a comprehensive explanation of the ECS pattern, so if you never heard of it, I suggest you to read one of the previously mentioned articles. Further, there is another one on *Gamedev.net* that explains the pattern[^gdev1] and a follow-up article that discusses an exemplary implementation of an ECS in *C*[^gdev2].

Using the ECS pattern, every entity is just a container for components. Components contain properties that define a particular trait of an entity. In the above inheritance example, a *Starship* entity would have a *Position* component, a *Rotation* component, a *Velocity* component and optionally *BeamWeapons* and *Cargo* components. There is no *Starship* class, but just the set of components that define a starship.

Since entities are just sets of components and components only contain data, the game logic has to be implemented somewhere. This is where the *systems* come into play. A system implements a particular part of game logic. A *PhysicsSystem* for example would handle gravity and collisions, while a *PlayerInputSystem* would collect input from the user and translate it into movement. Systems operate on components. Every system has a set of required component types that each entity has to contain in order to be processed by it. There is, for example, no need for a *PlayerInputSystem* to process a *Nebula* entity, since nebulas can't be moved by any player (nor by any AI). This way, every system only loops through the entities that contain the required set of components.

If you were taught about encapsulation and object-oriented programming, this way of thinking might feel unintuitive at a first glance. The data is not hidden inside an entity class, but distributed across multiple components that are exposed to systems. However, even though it seems to violate many doctrines that you have learned, it has some important benefits:

1. Decoupling

   Entities don't know anything about logic and the system also don't know anything about each other.

2. Performance

   Systems which are not interested in certain entities don't have to decide over and over again whether an entity should be processed. Only entities with the required component types are added to a certain system.

3. Flexibility

   All avilable components can be combined and parameterized in order to create new types of entities. This data-driven approach is especially useful if non-programmers should be able to add new features to the game. A scenario editor could, for example, provide a set of standard entity templates, but also allow for the creation of own entity types. This would give the user the possibility to create entities that no programmer has ever thought of.

   If combining existing components is not sufficient for a particular use case, it is also conceivable to add new systems and new component types by implementing plugins. That way, it would be relatively easy to add new functionality without changing the original game's code.

If you're still not convinced, remember that *OOP* is not a self purpose and that nobody should be dogmatic about its principles. Object-orientation solves many problems and is clearly a good paradigm for software development. To put it in a nutshell, one should always use the right tool for the right problem. In this case, I decided to implement an ECS in order to achieve a loose coupling and enough flexibility for modding the game.

I should mention that there are some existing libraries that implement entity-component-systems. I tried one called *Ashley*[^ashley]. Although it looked promising at a first glance, it turned out that it was much more than I needed. As such, I pulled it out again and started to implement my own components and systems. The first working draft took me about an hour. It turned out not to be rocket science. However, I might have to invest some more time on optimizing the code (batch allocation, cache locality, etc.), but I won't do this until I have to (*"premature optimization is the root of all evil"*, Donald Ervin Knuth 1974). For now, the straight-forward approach is just fine as it is and I will continue on wiring the parts of this game together.

Best regards,<br/>
Carsten

<br/>

[^ashley]: Ashley - A tiny entity framework in Java<br/> [https://github.com/libgdx/ashley](https://github.com/libgdx/ashley){:target="_blank"}
[^cowboy]: Cowboy Programming: Evolve Your Hierarchy<br/> [http://cowboyprogramming.com/2007/01/05/evolve-your-heirachy](http://cowboyprogramming.com/2007/01/05/evolve-your-heirachy){:target="_blank"}
[^gpp]: Game Programming Patterns: Component<br/> [http://gameprogrammingpatterns.com/component.html](http://gameprogrammingpatterns.com/component.html){:target="_blank"}
[^gdev1]: Gamedev.net: Understanding Component-Entity-Systems<br/> [https://www.gamedev.net/articles/programming/general-and-gameplay-programming/understanding-component-entity-systems-r3013](https://www.gamedev.net/articles/programming/general-and-gameplay-programming/understanding-component-entity-systems-r3013){:target="_blank"}
[^gdev2]: Gamedev.net: Implementing Component-Entity-Systems<br/> [https://www.gamedev.net/articles/programming/general-and-gameplay-programming/implementing-component-entity-systems-r3382/](https://www.gamedev.net/articles/programming/general-and-gameplay-programming/implementing-component-entity-systems-r3382/){:target="_blank"}

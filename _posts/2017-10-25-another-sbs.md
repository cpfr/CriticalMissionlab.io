---
layout: post
title:  "Another Starship Bridge Simulator"
date:   2017-10-25 10:49:09 +0200
categories: general
comments: true
---

# What is *CriticalMission*?

I have been a *Star Trek* fan since I was a child and I remember my sister and me creating our own control panels and starship bridges from paper for playing our individual space adventures. Several years later - in the meantime we were grown up - my sister told me about a computer game called *Artemis*[^artemis]. I was pretty excited about it, since it provided the players an immersive experience of steering a starship in a local area network. Every player had a station: *Helm*, *Weapons*, *Communication*, *Science* and *Engineering*. Every station had contributed its part to the mission and the real challenge was not the game itself, but the teamwork that was necessary to keep the ship alive. In order to not end up in chaos, panicky yelling at each other and maneuvering the ship into an asteroid field, we quickly figured out that we needed a captain. A captain who had an overview of the situation and who negotiated all possible options.

We soon imagined new ideas and features for the game. Although *Artemis* provides comprehensive modding capabilities, it is a commercial product that has to be purchased and whose source code is not available. This was the reason why the developers of *Empty Epsilon*[^epsilon] started their work on an open source starship bridge simulator. *Empty Epsilon* is licensed under the GPL license. That means that anyone can get the source code of the game and modify it in order to extend it or to create a new game from it.

Since I am a software developer, I can't play any game without analyzing its internal processes and the technology that is used. Both, *Artemis* and *Empty Epsilon* require all players to install an executable program on their computer. At the time of writing this text, *Artemis* is only available for *Windows* systems, *iOS* and *Android* (to be fair, it also runs pretty nicely with *Wine* on *Linux* systems). However, I thought that there had to be a better solution to make it easy for users of all platforms to play such a game. The answer was easy: Nowadays, most of the web browsers have extensive multimedia capabilities. My idea was to write a game server that could be run on just one computer, while all other devices could simply access the game via a web browser. I started to implement parts of a simple prototype, when I realized that I was not the only one with that idea. I stumbled across the commercial project *Starship Horizons*[^horizons], which has a similar vision: a starship bridge simulator that runs inside the browser. So you may ask: Why start another project like this? Well, first of all, because it's fun! I like the challenge and I have a goal in mind. The goal is not to compete with any of the other projects. It's more like trying a different approach. Like *Artemis*, *Starship Horizons* is a commercial project - so no source code here, either. I would like to create *Critical Mission* as a free starship bridge simulator that runs in anyone's browser and release the source code under a permissive open source license (not GPL, though, since I don't like that one). Once the foundation is laid and all basic features are implemented, I would love to share the code and see other people add their features.

The cool thing about running the game inside a browser is that the setup does require almost no effort. The server could, for instance, be installed on a *Raspberry Pi* computer[^raspi] that also seves as a WiFi access point. All that the players would have to do was to connect to that network and enter the URL into their browser. Since nowadays almost everyone carries a smartphone or tablet computer, this could be a great thing for a party.

I have plenty of ideas and I am very excited about this project, which I do in my spare time - aside from my full-time software developer job. I would like read your ideas and opinions in the comments. What do you think would be cool features (in addition to a classic combat simulation)? Transporter beams for stealing alien cargo? Cloaking devices for sneaking around enemies? Campaigns for extended storytelling? For now, I just wanted to introduce the project and give a brief overview about similar games. I will write down some more about my ideas and the technical implementation in the following blog posts and I would like to read some your comments.

Best regards,<br/>
Carsten

<br/>

[^artemis]: Artemis Spaceship Bridge Simulator:<br/>[http://artemis.eochu.com/](http://artemis.eochu.com/){:target="_blank"}
[^epsilon]: Empty Epsilon - Multiplayer Spaceship Bridge Simulator<br/> [http://daid.github.io/EmptyEpsilon/](http://daid.github.io/EmptyEpsilon/){:target="_blank"}
[^raspi]: Raspberry Pi<br/>[https://www.raspberrypi.org/](https://www.raspberrypi.org/){:target="_blank"}
[^horizons]: Starship Horizons - Advanced Bridge Simulator:<br/>[http://starshiphorizons.com/](http://starshiphorizons.com/){:target="_blank"}
